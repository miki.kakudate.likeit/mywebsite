<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>myPage</title>
    <!-- myPage.cssの読み込み -->
    <link href="css/user.css" rel="stylesheet" type="text/css" />
    <!-- header.cssの読み込み -->
    <link href="css/header.css" rel="stylesheet" type="text/css" />
    <!-- common.cssの読み込み -->
    <link href="css/common.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrapの読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/1b56a9b5b5.js" crossorigin="anonymous"></script>
</head>

<body>
    <!-- ヘッダー -->
    <header>
        <h2 class="title"><a href="Top">オンライン直売所</a></h2>
         <ul>
         
          <li><a href="Cart" class="btn4"><i class="fa-solid fa-cart-shopping"></i></a></li>
          <li><a href="Logout" class="btn4"><i class="fa-solid fa-right-from-bracket"></i></a></li>
         </ul>
    </header>

    <div class="container mt-5">
        <!-- ユーザ情報 -->
        <div>
            <div class="col-3 mx-auto">
                <h3>ユーザー情報</h3>
            </div>
            <div class="col-5 mx-auto userInfo">
             <form action="User" method="POST">
                <table>
                    <tbody>
                        <tr>
                            <td width="100" height="50">名前</td>
                            <td width="200"><input type="text" name="name" value="${userInfo.name}"></td>
                        </tr>
                        <tr>
                            <td width="100" height="50">メールアドレス</td>
                            <td><input type="text" name="mail" value="${userInfo.mail}"></td>
                        </tr>
                       
                    </tbody>
                </table>
                 <input type="hidden" name="id" value="${userInfo.id }">
                <div class="col-8 mx-auto"><input type="submit" class="btn btn-primary ml-5" value="更新"></div>
</form>
            </div>
        </div>

        <!-- 購入履歴 -->
        <div class="buy col-8 mx-auto mt-5">
            <table class="table">
                <thead class="thead-light">
                    <tr>
                        <th style="width: 10%"></th>
                        <th class="center">購入日時</th>
                        <th class="center">配送方法</th>
                        <th class="center">購入金額</th>
                    </tr>
                </thead >
                <c:forEach var="buyHistory" items="${buyHistoryList }">
                <tbody>

                    <tr>
                        <td class="center"><a href="BuyHistory?id=${buyHistory.id }" class="btn btn-info">詳細</a></td>
                        <td class="center">${buyHistory.buyDatetime}</td>
                        <td class="center">${buyHistory.deliveryName}円</td>
                        <td class="center">
                            ${buyHistory.totalPrice}円</td>
                    </tr>
                </tbody>
                </c:forEach>
            </table>
        </div>
    </div>

</body></html>
