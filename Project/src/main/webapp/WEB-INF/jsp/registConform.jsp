<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>home</title>
    <!-- header.cssの読み込み -->
    <link href="header.css" rel="stylesheet" type="text/css" />
    <!-- common.cssの読み込み -->
    <link href="common.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrapの読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>

<body>
   


    <div class="container mt-5">
        <div class="col-3 mx-auto">
            <h3>登録内容確認</h3>
        </div>
       
        <div class="col-5 mx-auto">
         <form action="RegistConfirm" method="POST">
            <table>
                <tbody>
                    <tr height="80">
                        <td class="center" width="200">名前</td>
                        <td class="center"><input type="text" name="name" value="${user.name }"></td>
                    </tr>
                    <tr height="80">
                        <td class="center">メールアドレス</td>
                        <td class="center"><input type="text" name="mail" value="${user.mail }"></td>
                    </tr>
                   
                    <tr height="80">
                        <td class="center">パスワード</td>
                        <td class="center"><input type="password" name="password" value="${user.password }"></td>
                    </tr>
                   
                </tbody>
            </table>
            <div class="col-3 mx-auto mt-3">
                <input type="submit" class="btn btn-primary" value="登録">
            </div>
            </form>
            <ul class="nav">
                <li class="nav-item">
                  <a class="nav-link active" href="Regist">修正</a>
                </li>
            
              </ul>
        </div>
    </div>

</body></html>
