<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>login</title>
<!-- login.cssの読み込み -->
<link href="css/login.css" rel="stylesheet">

<link href="css/common.css" rel="stylesheet">
<!-- Bootstrapの読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>

<body>
	<!-- ヘッダー -->

	<ul class="navbar-nav flex-row">
		<li class="nav-item"><a class="nav-link active" href="Regist">会員登録</a>
		</li>
	</ul>
	<div class="row">
		<div class="col-6 offset-3 mb-5">
			<c:if test="${errMsg != null}">
				<div class="alert alert-danger" role="alert">${errMsg}</div>
			</c:if>
		</div>
	</div>
	<!-- ログインフォーム -->
	<form class="form-signin mt-5" action="Login" method="POST">
		<div class="text-center mb-4">
			<h1 class="h3 mb-3">ログイン画面</h1>
		</div>
		<div class="col-6 mx-auto">
			<table>
				<tbody>
					<tr height="80">
						<td class="center" width="200">メールアドレス</td>
						<td class="center"><input type="text" name="loginid"
							value="${loginId}"></td>
					</tr>
					<tr height="80">
						<td class="center">パスワード</td>
						<td class="center"><input type="password" name="password"
							value="${password}"></td>
					</tr>


				</tbody>
			</table>
		</div>
		<div class="text-center mb-4">
			<input type="submit" class="btn btn-primary" name="action"
				value="ログイン">
		</div>
	</form>

</body>
</html>
