<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>home</title>
<!-- header.cssの読み込み -->
<link href="css/header.css" rel="stylesheet">
<link href="css/top.css" rel="stylesheet">
<!-- common.cssの読み込み -->
<link href="css/common.css" rel="stylesheet">
<!-- Bootstrapの読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<script src="https://kit.fontawesome.com/1b56a9b5b5.js"
	crossorigin="anonymous"></script>

</head>

<body>
	<header>
		<h2 class="title">
			<a href="Top">オンライン直売所</a>
		</h2>
		<ul>

			<li><a href="Cart" class="btn4"><i
					class="fa-solid fa-cart-shopping"></i></a></li>
			<li><a href="User" class="btn4"><i class="fa-solid fa-user"></i></a></li>
		</ul>
	</header>

	<div class="row">
		<div class="col s6">
			<div class="card">
				<div class="card-image">
					<img src="${itemDetail.imagePath }">
				</div>
			</div>
		</div>
		<div class="col s12 m3">
			<div class="card">
				<div class="card-image">
					<a href="ProducerDetail?id=${producer.id }"><img
						src="${producer.imagePath }" class="img-fluid"
						alt="Responsive image"></a>
				</div>
				<div class="card-content">
					<span class="card-title">${producerInfo.prefectureName }${producerInfo.municipalName }</span>
					<p>${producer.name }</p>
				</div>
			</div>
		</div>
		<div class="col s6">
			<h5>${producerInfo.prefectureName }${producerInfo.municipalName }</h5>
			<h4>${itemDetail.name }</h4>
			<h5>${itemDetail.price }円/${itemDetail.unit }</h5>
			<br>
			<form action="ItemAdd" method="POST">
				<div class="col s4">
					<input type="number" name="count" min="1" max="100"
						value="1" />個 
						<input type="hidden" name="id" value="${itemDetail.id }">
					<button class="btn waves-effect waves-light" type="submit">
							カートに入れる
						</button>
				</div>
			</form>
		</div>
	</div>
	<div class="container mt-5">
		<div class="col-3 mx-auto">
			<h5>${itemDetail.detail }</h5>
		</div>
	</div>
</body>
</html>
