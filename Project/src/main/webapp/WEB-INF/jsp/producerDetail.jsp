<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>home</title>
    <!-- header.cssの読み込み -->
    <link href="css/header.css" rel="stylesheet">
    <link href="css/top.css" rel="stylesheet">
    <!-- common.cssの読み込み -->
    <link href="css/common.css" rel="stylesheet">
    <!-- Bootstrapの読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/1b56a9b5b5.js" crossorigin="anonymous"></script>
   
</head>

<body>
    <header>
        <h2 class="title"><a href="Top">オンライン直売所</a></h2>
         <ul>
         
          <li><a href="Cart" class="btn4"><i class="fa-solid fa-cart-shopping"></i></a></li>
          <li><a href="User" class="btn4"><i class="fa-solid fa-user"></i></a></li>
         </ul>
        </header>
        <div class="row">
        <div class="col s7">
            <div class="card">
                <div class="card-image">
                   <img src="${producer.imagePath }">
                </div>
                <div class="card-content">
                    <span class="card-title">${producer.prefectureName }${producer.municipalName }</span>
                    <p>${producer.name}</p>
                </div>
            </div>
		</div>
        <div class="container">
            <div class="row center">
                <h5 class=" col s12 light">出品商品</h5>
            </div>
		<div class="section">
			<!--   おすすめ商品   -->
			<div class="row">
               <c:forEach var="item" items="${itemList }">
				<div class="col s12 m3">
					<div class="card">
						<div class="card-image">
							<a href="ItemDetail?id=${item.id }"><img src="${item.imagePath }" class="img-fluid" alt="Responsive image"></a>
						</div>
						<div class="card-content">
							<span class="card-title">${item.name }</span>
							<p>${item.price }円/${item.unit }
							</p>
						</div>
					</div>
				</div>
				</c:forEach>
				
			</div>
		</div>
	</div>
</div>

</body></html>
