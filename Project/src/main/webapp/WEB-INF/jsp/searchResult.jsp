<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>home</title>
<!-- header.cssの読み込み -->
<link href="css/header.css" rel="stylesheet">
<link href="css/top.css" rel="stylesheet">
<!-- common.cssの読み込み -->
<link href="css/common.css" rel="stylesheet">
<!-- Bootstrapの読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<script src="https://kit.fontawesome.com/1b56a9b5b5.js"
	crossorigin="anonymous"></script>


</head>

<body>
	<header>
		<h2 class="title">
			<a href="Top">オンライン直売所</a>
		</h2>
		<ul>

			<li><a href="Cart" class="btn4"><i
					class="fa-solid fa-cart-shopping"></i></a></li>
			<li><a href="User" class="btn4"><i class="fa-solid fa-user"></i></a></li>
		</ul>
	</header>
	<div class="row">
		<div class="container mt-5">
			<div class="col-3 mx-auto">
				<h1>検索結果一覧</h1>
			</div>

		</div>

		<div class="container">
			<div class="section">
				<div class="card-columns">
					<c:if test="${searchItems!=null }">
						<c:forEach var="searchItem" items="${searchItems}"
							varStatus="status">

							
								<div class="card">
									<div class="card-image">
										<a href="ItemDetail?id=${searchItem.id }"><img
											src="${searchItem.imagePath }" class="img-fluid"
											alt="Responsive image"></a>
									</div>
									<div class="card-content">
										<span class="card-title">${searchItem.name}</span>
										<p>${searchItem.price }円/${searchItem.unit }</p>

									</div>
								</div>
					</c:forEach>
					</c:if>

					<c:if test="${searchProducers!=null }">
						<c:forEach var="searchProducer" items="${searchProducers}">
							<div class="col s12 m3">
								<div class="card">
									<div class="card-image">
										<a href="ProducerDetail?id=${searchProducer.id }"><img
											src="${searchProducer.imagePath }" class="img-fluid"
											alt="Responsive image"></a>
									</div>
									<div class="card-content">
										<span class="card-title">${searchProducer.prefectureName }${searchProducer.municipalName }</span>
										<p>${searchProducer.name }</p>
									</div>
								</div>
							</div>
						</c:forEach>
					</c:if>
				</div>
			</div>
		</div>


	</div>
</body>
</html>
