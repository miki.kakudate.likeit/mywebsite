<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>home</title>
    <!-- header.cssの読み込み -->
    <link href="css/header.css" rel="stylesheet">
    <link href="css/top.css" rel="stylesheet">
    <!-- common.cssの読み込み -->
    <link href="css/common.css" rel="stylesheet">
    <!-- Bootstrapの読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/1b56a9b5b5.js" crossorigin="anonymous"></script>
   
   
</head>

<body>
    <header>
        <h2 class="title"><a href="Top">オンライン直売所</a></h2>
         <ul>
         
			<li><a href="Cart" class="btn4"><i class="fa-solid fa-cart-shopping"></i></a></li>
			<li><a href="User" class="btn4"><i class="fa-solid fa-user"></i></a></li>
         </ul>
        </header>
   <div class="row">
        <div class="container mt-5">
            <div class="col-3 mx-auto">
                <h1>${category.name}一覧</h1>
            </div>
            <form action="ImperfectItem" method="POST">
            <input type="hidden" name="category_id" value="${category.id }">
			<p><input type="submit" class="btn btn-info" value="訳アリ"></p>
			</form>
		</div>
      <div class="container">
		
		<div class="section">
			<!--   おすすめ商品   -->
			<div class="row">
				 <c:forEach var="index" items="${indexList}">
				<div class="col s12 m3">
					<div class="card">
						<div class="card-image">
							<a href="ItemDetail?id=${index.id }"><img src="${index.imagePath }" class="img-fluid" alt="Responsive image"></a>
						</div>
						<div class="card-content">
							<span class="card-title">${index.name}</span>
							<p>${index.price }円/${index.unit }
							</p>
						</div>
					</div>
				</div>
				</c:forEach>
				
				
			</div>
		</div>
	</div>
	
   
</div>
</body></html>
