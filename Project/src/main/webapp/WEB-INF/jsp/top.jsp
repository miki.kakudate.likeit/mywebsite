<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>top</title>
<!-- header.cssの読み込み -->
<link href="css/header.css" rel="stylesheet">
<link href="css/top.css" rel="stylesheet">
<!-- common.cssの読み込み -->
<link href="css/common.css" rel="stylesheet">
<!-- Bootstrapの読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<script src="https://kit.fontawesome.com/1b56a9b5b5.js"
	crossorigin="anonymous"></script>

</head>

<body>
	<header>
		<h2 class="title">
			<a href="Top">オンライン直売所</a>
		</h2>
		<ul>

			<li><a href="Cart" class="btn4"><i
					class="fa-solid fa-cart-shopping"></i></a></li>
			<li><a href="User" class="btn4"><i class="fa-solid fa-user"></i></a></li>
		</ul>
	</header>

	<div class="c">
		<main class="main">
			<div class="container">
				<div class="row center">
					<h5 class=" col s12 light">おすすめ野菜</h5>
				</div>
				<div class="section">
					<!--   おすすめ商品   -->
					<div class="row">
						<c:forEach var="vege" items="${vegeList}">
							<div class="col s12 m3">
								<div class="card">
									<div class="card-image">
										<a href="ItemDetail?id=${vege.id}"><img
											src="${vege.imagePath}" class="img-fluid"
											alt="Responsive image"></a>
									</div>
									<div class="card-content">
										<span class="card-title">${vege.name}</span>
										<p>${vege.price}円/${vege.unit}</p>
									</div>
								</div>
							</div>
						</c:forEach>


					</div>
				</div>
			</div>
			<div class="container">
				<div class="row center">
					<h5 class=" col s12 light">おすすめ果物</h5>
				</div>
				<div class="section">
					<!--   おすすめ商品   -->
					<div class="row">
						<c:forEach var="fruit" items="${fruitsList}">
							<div class="col s12 m3">
								<div class="card">
									<div class="card-image">
										<a href="ItemDetail?id=${fruit.id}"><img
											src="${fruit.imagePath}" class="img-fluid"
											alt="Responsive image"></a>
									</div>
									<div class="card-content">
										<span class="card-title">${fruit.name}</span>
										<p>${fruit.price}円/${fruit.unit}</p>
									</div>
								</div>
							</div>
						</c:forEach>


					</div>
				</div>
			</div>
		</main>
		<aside class="side">
			<form action="Search" method="POST">
				<div class="input-group mb-3">
					<input type="text" class="form-control" placeholder="キーワード検索"
						aria-label="Recipient's username" aria-describedby="button-addon2"
						value="${search_word}">
					<div class="input-group-append">
						<button class="btn btn-outline-secondary" type="button"
							id="button-addon2">
							<i class="fas fa-search"></i>
						</button>
					</div>
				</div>

				<p>
					<input type="checkbox" name="item" value="item">商品
				</p>
				<p>
					<input type="checkbox" name="producer" value="producer">生産者
				</p>
			</form>
			<ul class="nav">
				<li class="nav-item">
					<h6>・野菜</h6> <c:forEach var="category" items="${categoryList}">
						<a class="nav-link active" href="Index?id=${category.id }">・${category.name}</a>
					</c:forEach> <a class="nav-link active" href="ProducerIndex">・生産者</a>

				</li>

			</ul>
		</aside>
	</div>
</body>
</html>
