<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>myPage</title>
    <!-- myPage.cssの読み込み -->
    <link href="css/user.css" rel="stylesheet" type="text/css" />
    <!-- header.cssの読み込み -->
    <link href="css/header.css" rel="stylesheet" type="text/css" />
    <!-- common.cssの読み込み -->
    <link href="css/common.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrapの読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/1b56a9b5b5.js" crossorigin="anonymous"></script>
</head>

<body>
    <!-- ヘッダー -->
    <header>
        <h2 class="title"><a href="Top">オンライン直売所</a></h2>
         <ul>
         
          <li><a href="Cart" class="btn4"><i class="fa-solid fa-cart-shopping"></i></a></li>
          <li><a href="User" class="btn4"><i class="fa-solid fa-user"></i></a></li>
         </ul>
    </header>

    <div class="container mt-5">
       
        <div>
            <div class="col-2 mx-auto">
                <h3>履歴詳細</h3>
            </div>
            
        </div>

        <!-- 購入履歴 -->
        <div class="buy col-6 mx-auto mt-5">
            <table class="table" style="width: 100%">
                <thead>
                    <tr>
                        
                        <th class="center">購入日時</th>
                        <th class="center" style="width: 30%">配送方法</th>
                        <th class="center" style="width: 30%">購入金額</th>
                    </tr>
                </thead>
                <tbody>

                    <tr>
                        <td class="center">${buyHistory.buyDatetime}</td>
                        <td class="center">${buyHistory.deliveryName}</td>
                        <td class="center">${buyHistory.totalPrice }円</td>
                    </tr>
                </tbody>
            </table>
            </div>
            <div class="buy col-6 mx-auto mt-5">
                <table class="table" style="width: 100%">
                    <thead>
                        <tr>
                           
                            <th class="center">購入商品</th>
                            <th class="center" style="width: 30%">単価</th>
                            <th class="center" style="width: 30%">個数</th>
                        </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="buyItem" items="${buyItemList}">
                        <tr>
                           <td>
                                <div class="card-image">
                                    <a href="ItemDetail?id=${buyItem.itemId }"><img src="${buyItem.itemImagePath}" class="img-fluid" alt="Responsive image"></a>
                                </div>
                                <div class="card-content">
                                    <span class="card-title">${buyItem.itemName}</span>
                                </div>
                            </td>
                            <td class="center">${buyItem.itemPrice}円/${buyItem.itemUnit}</td>
                            <td class="center">
                                ${buyItem.count}個</td>
                        </tr>
                        </c:forEach>
                    </tbody>
                </table>
        </div>
    </div>

</body></html>
