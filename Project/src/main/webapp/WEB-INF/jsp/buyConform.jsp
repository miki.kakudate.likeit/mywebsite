<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>myPage</title>
    <!-- myPage.cssの読み込み -->
    <link href="css/user.css" rel="stylesheet" type="text/css" />
    <!-- header.cssの読み込み -->
    <link href="css/header.css" rel="stylesheet" type="text/css" />
    <!-- common.cssの読み込み -->
    <link href="css/common.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrapの読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/1b56a9b5b5.js" crossorigin="anonymous"></script>
</head>

<body>
    <!-- ヘッダー -->
    <header>
        <h2 class="title"><a href="Top">オンライン直売所</a></h2>
         <ul>
         
          <li><a href="Cart" class="btn4"><i class="fa-solid fa-cart-shopping"></i></a></li>
          <li><a href="User" class="btn4"><i class="fa-solid fa-user"></i></a></li>
          
         </ul>
    </header>
    <div class="container mt-5">
       
        <div>
            <div class="col-4 mx-auto">
                <h3>購入が完了しました</h3>
            </div>
            
        </div>
        <div class="col-2 mx-auto">
            <input type="button" class="btn btn-primary" onClick="location.href='Top'" value="トップへ">
        </div>
    </div>
</body>
</html>
