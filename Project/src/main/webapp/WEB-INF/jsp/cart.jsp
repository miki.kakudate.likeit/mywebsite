<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>myPage</title>
    <!-- myPage.cssの読み込み -->
    <link href="css/user.css" rel="stylesheet" type="text/css" />
    <!-- header.cssの読み込み -->
    <link href="css/header.css" rel="stylesheet" type="text/css" />
    <!-- common.cssの読み込み -->
    <link href="css/common.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrapの読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/1b56a9b5b5.js" crossorigin="anonymous"></script>
</head>

<body>
    <!-- ヘッダー -->
    <header>
        <h2 class="title"><a href="Top">オンライン直売所</a></h2>
         <ul>
         
          <li><a href="Cart" class="btn4"><i class="fa-solid fa-cart-shopping"></i></a></li>
          <li><a href="User" class="btn4"><i class="fa-solid fa-user"></i></a></li>
          
         </ul>
    </header>


    <div class="container mt-5">
       
        <div>
            <div class="col-2 mx-auto">
                <h3>カート</h3>
            </div>
            
        </div>
        <c:if test="${cartList == null}">
<div class="container mt-5">
            <div class="col-5 mx-auto">
                <div class="alert alert-primary" role="alert">
                <h3>カートに商品がありません</h3>
            </div>
        </div>
        <div class="col-2 mx-auto">
            <input type="button" class="btn btn-primary" onClick="location.href='Top'" value="買い物を続ける">
        </div>
    </div>
    </c:if>
        <!-- 購入履歴 -->
        <c:if test="${cartList != null}">
        <form action="Buy" method="POST">
        <div class="buy col-8 mx-auto mt-5">
            <table style="width: 120%">
                <thead>
                    <tr>
                       
                        <th class="center" style="width: 25%">商品</th>
                        <th class="center" style="width: 25%">単価</th>
                        <th class="center" style="width: 25%">個数</th>
                        <th class="center" style="width: 25%">小計</th>
                    </tr>
                </thead>
                
                <tbody>
                 <c:forEach var="item" items="${cartList}">
                 <td class="center"><a href="DeleteCartItem?id=${item.id}">削除</a></td>
                    <tr>
                        <td class="center"><div class="card">
                            <div class="card-image">
                                <a href="Index?id=${item.id }"><img src="${item.imagePath }" class="img-fluid" alt="Responsive image"></a>
                            </div>
                            <div class="card-content">
                                <span class="card-title">${item.name }</span>
                            </div>
                        </div></td>
                        <td class="center">${item.price }円/${item.unit }</td>
                        <td class="center">
                            ${item.count }個</td>
                            <td class="center">${item.price*item.count }円</td>
                    </tr>
                     </c:forEach>
                </tbody>
                <tfoot>
                    <tr>
                        <td class="center">
                            <div class="input-field col s8">
                                <label>配送方法</label>
                                <select name="delivery_id">
                                 <c:forEach var="delivery" items="${deliveryList}">
                                        <option value="${delivery.id }">${delivery.name}${delivery.price }</option>
                                         </c:forEach>
                                </select> 
                            </div>
                        </td>
                    
                    </tr>
                    </tfoot>
            </table>
            </div>
            
           
            
           <div class="row">
                <div class="col-1 mx-auto">
                    <input type="button" class="btn btn-primary" onClick="location.href='Top'" value="買い物を続ける">
                </div>
                 <div class="col-2 mx-auto">
            <input type="submit" class="btn btn-primary" value="購入確認">
        </div>
        </div>
        </form>
        </c:if>
    </div>
   

</body></html>
