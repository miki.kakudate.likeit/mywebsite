package servret;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import dao.UserDao;

/**
 * Servlet implementation class RegistConfirm
 */
@WebServlet("/RegistConfirm")
public class RegistConfirm extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegistConfirm() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      request.setCharacterEncoding("UTF-8");

      String name = request.getParameter("name");
      String mail = request.getParameter("mail");
      String password = request.getParameter("password");

      try { // 暗号化
        MessageDigest md5 = MessageDigest.getInstance("MD5");

        byte[] result = md5.digest(password.getBytes());

        int[] i = new int[result.length];
        StringBuffer sb = new StringBuffer();
        for (int j = 0; j < result.length; j++) {
          i[j] = (int) result[j] & 0xff;
          if (i[j] <= 15) {
            sb.append("0");
          }
          sb.append(Integer.toHexString(i[j]));
        }
        String inputPass = sb.toString();
        UserDao.userRegist(name, mail, inputPass);
      } catch (NoSuchAlgorithmException x) {

      }
      response.sendRedirect("Login");
	}

}
