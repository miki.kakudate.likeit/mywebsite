package servret;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import beans.CategoryBeans;
import beans.ItemsBeans;
import beans.ProducerBeans;
import dao.CategoryDao;
import dao.ItemDao;
import dao.ProducerDao;

/**
 * Servlet implementation class Top
 */
@WebServlet("/Top")
public class Top extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Top() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      try {
        ArrayList<ItemsBeans> vegeList = ItemDao.getRandVegetables(4);
        ArrayList<ItemsBeans> fruitsList = ItemDao.getRandFruits(4);
        ArrayList<CategoryBeans> categoryList = CategoryDao.getAll();

        request.setAttribute("vegeList", vegeList);
        request.setAttribute("fruitsList", fruitsList);
        request.setAttribute("categoryList", categoryList);
      } catch (SQLException e) {
        e.printStackTrace();
      }
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/top.jsp");// フォワード
      dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      request.setCharacterEncoding("UTF-8");

      String searchWord = request.getParameter("search_word");
      String producer = request.getParameter("producer");
      String item = request.getParameter("item");

      if (producer == null) {
        ArrayList<ItemsBeans> searchResult = ItemDao.searchItems(searchWord);
        request.setAttribute("searchResult", searchResult);
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/top.jsp");// フォワード
        dispatcher.forward(request, response);
      }
      if (item == null) {
        ArrayList<ProducerBeans> searchResult = ProducerDao.searchProducers(searchWord);
        request.setAttribute("searchResult", searchResult);
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/top.jsp");// フォワード
      dispatcher.forward(request, response);
    }
	}

}
