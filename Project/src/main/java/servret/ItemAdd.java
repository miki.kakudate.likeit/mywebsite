package servret;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import beans.DeliveryBeans;
import beans.ItemsBeans;
import beans.UserBeans;
import dao.DeliveryDao;
import dao.ItemDao;

/**
 * Servlet implementation class ItemAdd
 */
@WebServlet("/ItemAdd")
public class ItemAdd extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ItemAdd() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      request.setCharacterEncoding("UTF-8");
      HttpSession session = request.getSession();
      UserBeans userInfo = (UserBeans) session.getAttribute("userInfo");

      if (userInfo == null) {
        response.sendRedirect("Login");
        return;
      }

      String id = request.getParameter("id");
      String count = request.getParameter("count");
      int itemId = Integer.valueOf(id);
      int itemCount = Integer.valueOf(count);


      ItemsBeans item = ItemDao.findById(itemId);


      ArrayList<ItemsBeans> cartList = (ArrayList<ItemsBeans>) session.getAttribute("cartList");

      if (cartList == null) {
        cartList = new ArrayList<ItemsBeans>();
      }

      item.setCount(itemCount);
      cartList.add(item);

      ArrayList<DeliveryBeans> deliveryList = DeliveryDao.findAll();

      request.setAttribute("deliveryList", deliveryList);
      session.setAttribute("cartList", cartList);




      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/cart.jsp");// フォワード
      dispatcher.forward(request, response);

	}

}
