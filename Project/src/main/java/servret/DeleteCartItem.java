package servret;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import beans.ItemsBeans;

/**
 * Servlet implementation class DeliteCartItem
 */
@WebServlet("/DeleteCartItem")
public class DeleteCartItem extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteCartItem() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
      HttpSession session = request.getSession();
      ArrayList<ItemsBeans> cartList = (ArrayList<ItemsBeans>) session.getAttribute("cartList");
      int deleteItemId = Integer.valueOf(request.getParameter("id"));

      for (ItemsBeans item : cartList) {
        if (item.getId() == deleteItemId) {
          cartList.remove(item);
        }
        break;
      }

      session.setAttribute("cartList", cartList);
      response.sendRedirect("Cart");

    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
      // TODO Auto-generated method stub
      doGet(request, response);
	}

}
