package servret;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import beans.ItemsBeans;
import beans.ProducerBeans;
import beans.UserBeans;
import dao.ItemDao;
import dao.ProducerDao;

/**
 * Servlet implementation class ItemDetail
 */
@WebServlet("/ItemDetail")
public class ItemDetail extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public ItemDetail() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession();
    UserBeans userInfo = (UserBeans) session.getAttribute("userInfo");

    if (userInfo == null) {
      response.sendRedirect("Login");
      return;
    }
    String i = request.getParameter("id");
    int itemId = Integer.valueOf(i);

    ItemsBeans itemDetail = ItemDao.findById(itemId);
    ProducerBeans producer = ProducerDao.findByItemId(itemId);

    int producerId = producer.getId();
    ProducerBeans producerInfo = ProducerDao.findById(producerId);

    request.setAttribute("itemDetail", itemDetail);
    request.setAttribute("producer", producer);
    request.setAttribute("producerInfo", producerInfo);
    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/itemDetail.jsp");// フォワード
    dispatcher.forward(request, response);
  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // TODO Auto-generated method stub
    doGet(request, response);
  }

}
