package servret;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import beans.DeliveryBeans;
import beans.ItemsBeans;
import beans.UserBeans;
import dao.DeliveryDao;
import dao.ItemDao;

/**
 * Servlet implementation class Buy
 */
@WebServlet("/Buy")
public class Buy extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public Buy() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    request.setCharacterEncoding("UTF-8");
    HttpSession session = request.getSession();
    UserBeans userInfo = (UserBeans) session.getAttribute("userInfo");

    if (userInfo == null) {
      response.sendRedirect("Login");
      return;
    }

    String id = request.getParameter("delivery_id");
    int deliveryId = Integer.valueOf(id);

    DeliveryBeans delivery = DeliveryDao.findById(deliveryId);

    ArrayList<ItemsBeans> cartItem = (ArrayList<ItemsBeans>) session.getAttribute("cartList");

    int totalPrice = ItemDao.getTotalPrice(cartItem);
    int totalCount = ItemDao.getTotalCount(cartItem);
    totalPrice += delivery.getPrice();

    request.setAttribute("delivery", delivery);
    request.setAttribute("totalPrice", totalPrice);
    request.setAttribute("totalCount", totalCount);

    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/buy.jsp");// フォワード
    dispatcher.forward(request, response);
  }

}
