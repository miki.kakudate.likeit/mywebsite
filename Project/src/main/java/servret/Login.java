package servret;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import beans.UserBeans;
import dao.UserDao;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");// フォワード
      dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      request.setCharacterEncoding("UTF-8");

      String loginId = request.getParameter("loginid");
      String password = request.getParameter("password");
      UserDao userDao = new UserDao();
      UserBeans userInfo = new UserBeans();

      try { // 暗号化
        MessageDigest md5 = MessageDigest.getInstance("MD5");

        byte[] result = md5.digest(password.getBytes());

        int[] h = new int[result.length];
        StringBuffer sb = new StringBuffer();
        for (int j = 0; j < result.length; j++) {
          h[j] = (int) result[j] & 0xff;
          if (h[j] <= 15) {
            sb.append("0");
          }
          sb.append(Integer.toHexString(h[j]));
        }
        String pass = sb.toString();

        userInfo = userDao.findByLoginInfo(loginId, pass);

      } catch (NoSuchAlgorithmException x) {

      }

      if (userInfo == null) {
        request.setAttribute("errMsg", "メールアドレスまたはパスワードが異なります");
        request.setAttribute("loginId", loginId);// リクエスト領域
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
        dispatcher.forward(request, response);
        return;
      }

      HttpSession session = request.getSession();
      session.setAttribute("userInfo", userInfo);// セッション領域

      response.sendRedirect("Top");
    }



}
