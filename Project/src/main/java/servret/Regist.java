package servret;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import beans.UserBeans;

/**
 * Servlet implementation class Regist
 */
@WebServlet("/Regist")
public class Regist extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Regist() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/regist.jsp");// フォワード
      dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      request.setCharacterEncoding("UTF-8");

      String name = request.getParameter("name");
      String mail = request.getParameter("mail");
      String password = request.getParameter("password");
      String passwordConfirm = request.getParameter("password_confirm");

      UserBeans user = new UserBeans();

      // エラー後入力したテキストを表示
      user.setName(name);
      user.setMail(mail);
      user.setPassword(password);

      if (name.equals("") || mail.equals("") || password.equals("") || passwordConfirm.equals("")||!password.equals(passwordConfirm)) {
        request.setAttribute("errMsg", "入力された内容は正しくありません");

        request.setAttribute("user", user);
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/regist.jsp");
        dispatcher.forward(request, response);
        return;
      }
      
      request.setAttribute("user", user);

      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/registConform.jsp");// フォワード
      dispatcher.forward(request, response);


	}

}
