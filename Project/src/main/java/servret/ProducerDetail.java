package servret;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import beans.ItemsBeans;
import beans.ProducerBeans;
import dao.ItemDao;
import dao.ProducerDao;

/**
 * Servlet implementation class ProducerDetail
 */
@WebServlet("/ProducerDetail")
public class ProducerDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProducerDetail() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      int producerId = Integer.valueOf(request.getParameter("id"));

      ProducerBeans producer = ProducerDao.findById(producerId);
      ArrayList<ItemsBeans> itemList = ItemDao.findByProducerId(producerId);

      request.setAttribute("producer", producer);
      request.setAttribute("itemList", itemList);

      RequestDispatcher dispatcher =
          request.getRequestDispatcher("/WEB-INF/jsp/producerDetail.jsp");// フォワード
      dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
