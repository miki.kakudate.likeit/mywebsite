package servret;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import beans.ItemsBeans;
import beans.ProducerBeans;
import dao.ItemDao;
import dao.ProducerDao;

/**
 * Servlet implementation class Search
 */
@WebServlet("/Search")
public class Search extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Search() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      request.setCharacterEncoding("UTF-8");
      String searchWord = request.getParameter("search_word");
      String item = request.getParameter("item");
      String producer = request.getParameter("producer");

      if (item != null) {
        ArrayList<ItemsBeans> searchItems = ItemDao.searchItems(searchWord);
        request.setAttribute("searchItems", searchItems);
      }
      if (producer != null) {
        ArrayList<ProducerBeans> searchProducers = ProducerDao.searchProducers(searchWord);
        request.setAttribute("searchProducers", searchProducers);
      }

      if (item == null && producer == null) {
        ArrayList<ItemsBeans> searchItems = ItemDao.searchItems(searchWord);
        request.setAttribute("searchItems", searchItems);
        ArrayList<ProducerBeans> searchProducers = ProducerDao.searchProducers(searchWord);
        request.setAttribute("searchProducers", searchProducers);
      }


      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/searchResult.jsp");// フォワード
      dispatcher.forward(request, response);
	}

}
