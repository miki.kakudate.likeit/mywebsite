package servret;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import beans.CategoryBeans;
import beans.ItemsBeans;
import dao.CategoryDao;
import dao.ItemDao;

/**
 * Servlet implementation class ImperfectItem
 */
@WebServlet("/ImperfectItem")
public class ImperfectItem extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ImperfectItem() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      request.setCharacterEncoding("UTF-8");

      int categoryId = Integer.valueOf(request.getParameter("category_id"));

      CategoryBeans category = CategoryDao.findById(categoryId);
      ArrayList<ItemsBeans> imperfectItemList = ItemDao.findImperfectitem(categoryId);

      request.setAttribute("category", category);
      request.setAttribute("imperfectItemList", imperfectItemList);

      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/imperfectItem.jsp");// フォワード
      dispatcher.forward(request, response);
	}

}
