package servret;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import beans.BuyHistoryBeans;
import beans.UserBeans;
import dao.BuyHistoryDao;
import dao.UserDao;

/**
 * Servlet implementation class User
 */
@WebServlet("/User")
public class User extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public User() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      request.setCharacterEncoding("UTF-8");
      HttpSession session = request.getSession();
      UserBeans userInfo = (UserBeans) session.getAttribute("userInfo");

      if (userInfo == null) {
        response.sendRedirect("Login");
        return;
      }

      UserBeans user = UserDao.findById(userInfo.getId());
      userInfo.setName(user.getName());
      userInfo.setMail(user.getMail());
      ArrayList<BuyHistoryBeans> buyHistoryList = BuyHistoryDao.findByUserId(userInfo.getId());

      request.setAttribute("userInfo", userInfo);
      request.setAttribute("buyHistoryList", buyHistoryList);

      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user.jsp");// フォワード
      dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      request.setCharacterEncoding("UTF-8");
      int id = Integer.valueOf(request.getParameter("id"));
      String name = request.getParameter("name");
      String mail = request.getParameter("mail");

      UserDao.userUpdate(name, mail, id);
      response.sendRedirect("User");
	}

}
