package servret;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import beans.BuyHistoryBeans;
import beans.BuyItemBeans;
import beans.UserBeans;
import dao.BuyHistoryDao;
import dao.BuyItemDao;

/**
 * Servlet implementation class BuyDetail
 */
@WebServlet("/BuyHistory")
public class BuyHistory extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BuyHistory() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      HttpSession session = request.getSession();
      UserBeans userInfo = (UserBeans) session.getAttribute("userInfo");

      if (userInfo == null) {
        response.sendRedirect("Login");
        return;
      }

      int buyHistoryId = Integer.valueOf(request.getParameter("id"));
      BuyHistoryBeans buyHistory = BuyHistoryDao.findById(buyHistoryId);
      ArrayList<BuyItemBeans> buyItemList = BuyItemDao.findByBuyHistoryId(buyHistoryId);

      request.setAttribute("buyHistory", buyHistory);
      request.setAttribute("buyItemList", buyItemList);

      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/buyHistory.jsp");// フォワード
      dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
