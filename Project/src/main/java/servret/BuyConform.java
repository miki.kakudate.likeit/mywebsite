package servret;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import beans.BuyItemBeans;
import beans.ItemsBeans;
import beans.UserBeans;
import dao.BuyHistoryDao;
import dao.BuyItemDao;

/**
 * Servlet implementation class BuyConform
 */
@WebServlet("/BuyConform")
public class BuyConform extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public BuyConform() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // TODO Auto-generated method stub
    response.getWriter().append("Served at: ").append(request.getContextPath());
  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    request.setCharacterEncoding("UTF-8");
    HttpSession session = request.getSession();
    UserBeans userInfo = (UserBeans) session.getAttribute("userInfo");

    if (userInfo == null) {
      response.sendRedirect("Login");
      return;
    }
    ArrayList<ItemsBeans> cartList = (ArrayList<ItemsBeans>) session.getAttribute("cartList");

    int totalPrice = Integer.valueOf(request.getParameter("totalPrice"));
    int deliveryId = Integer.valueOf(request.getParameter("delivery_id"));
    Long datetime = System.currentTimeMillis();
    Timestamp buyDate = new Timestamp(datetime);


    int buyHistoryId =
        BuyHistoryDao.insertBuyHistory(userInfo.getId(), deliveryId, buyDate, totalPrice);

    for (ItemsBeans cartItem : cartList) {
      BuyItemBeans buyItem = new BuyItemBeans();
      buyItem.setItemId(cartItem.getId());
      buyItem.setCount(cartItem.getCount());
      buyItem.setBuyHistoryId(buyHistoryId);
      BuyItemDao.insertBuyItem(buyItem);

    }

    session.removeAttribute("cartList");
    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/buyConform.jsp");// フォワード
    dispatcher.forward(request, response);
  }

}
