package beans;

import java.io.Serializable;

public class BuyItemBeans implements Serializable {
  private int id;
  private int itemId;
  private int count;
  private int buyHistoryId;
  private int itemPrice;
  private String itemName;
  private String itemUnit;
  private String itemImagePath;

  public BuyItemBeans(int id, int itemId, int count, int buyHistoryId) {
    super();
    this.id = id;
    this.itemId = itemId;
    this.count = count;
    this.buyHistoryId = buyHistoryId;
  }

  public BuyItemBeans(int id, int itemId, int count, int buyHistoryId, int itemPrice,
      String itemName, String itemUnit, String itemImagePath) {
    super();
    this.id = id;
    this.itemId = itemId;
    this.count = count;
    this.buyHistoryId = buyHistoryId;
    this.itemPrice = itemPrice;
    this.itemName = itemName;
    this.itemUnit = itemUnit;
    this.itemImagePath = itemImagePath;
  }

  public BuyItemBeans() {
    super();
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public int getItemId() {
    return itemId;
  }

  public void setItemId(int itemId) {
    this.itemId = itemId;
  }

  public int getCount() {
    return count;
  }

  public void setCount(int count) {
    this.count = count;
  }

  public int getBuyHistoryId() {
    return buyHistoryId;
  }

  public void setBuyHistoryId(int buyHistoryId) {
    this.buyHistoryId = buyHistoryId;
  }

  public int getItemPrice() {
    return itemPrice;
  }

  public void setItemPrice(int itemPrice) {
    this.itemPrice = itemPrice;
  }

  public String getItemName() {
    return itemName;
  }

  public void setItemName(String itemName) {
    this.itemName = itemName;
  }

  public String getItemImagePath() {
    return itemImagePath;
  }

  public void setItemImagePath(String itemImagePath) {
    this.itemImagePath = itemImagePath;
  }

  public String getItemUnit() {
    return itemUnit;
  }

  public void setItemUnit(String itemUnit) {
    this.itemUnit = itemUnit;
  }

}
