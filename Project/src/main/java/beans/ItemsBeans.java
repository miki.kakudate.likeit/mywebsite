package beans;

import java.io.Serializable;

public class ItemsBeans implements Serializable {
  private int id;
  private int producerId;
  private int categoryId;
  private String name;
  private int price;
  private String detail;
  private String unit;
  private String imagePath;
  private boolean isImperfectItem;
  private int count;

  public ItemsBeans() {}


  public ItemsBeans(int id, int producerId, int categoryId, String name, int price, String detail,
      String unit, String imagePath, boolean isImperfectItem) {
    super();
    this.id = id;
    this.producerId = producerId;
    this.categoryId = categoryId;
    this.name = name;
    this.price = price;
    this.detail = detail;
    this.unit = unit;
    this.imagePath = imagePath;
    this.isImperfectItem = isImperfectItem;
  }


  public ItemsBeans(int id, int producerId, int categoryId, String name, int price, String detail,
      String unit, String imagePath, boolean isImperfectItem, int count) {
    super();
    this.id = id;
    this.producerId = producerId;
    this.categoryId = categoryId;
    this.name = name;
    this.price = price;
    this.detail = detail;
    this.unit = unit;
    this.imagePath = imagePath;
    this.isImperfectItem = isImperfectItem;
    this.count = count;
  }


  public int getId() {
    return id;
  }


  public void setId(int id) {
    this.id = id;
  }


  public int getProducerId() {
    return producerId;
  }


  public void setProducerId(int producerId) {
    this.producerId = producerId;
  }


  public int getCategoryId() {
    return categoryId;
  }


  public void setCategoryId(int categoryId) {
    this.categoryId = categoryId;
  }


  public String getName() {
    return name;
  }


  public void setName(String name) {
    this.name = name;
  }


  public int getPrice() {
    return price;
  }


  public void setPrice(int price) {
    this.price = price;
  }


  public String getDetail() {
    return detail;
  }


  public void setDetail(String detail) {
    this.detail = detail;
  }


  public String getUnit() {
    return unit;
  }


  public void setUnit(String unit) {
    this.unit = unit;
  }


  public String getImagePath() {
    return imagePath;
  }


  public void setImagePath(String imagePath) {
    this.imagePath = imagePath;
  }


  public boolean isImperfectItem() {
    return isImperfectItem;
  }


  public void setImperfectItem(boolean isImperfectItem) {
    this.isImperfectItem = isImperfectItem;
  }


  public int getCount() {
    return count;
  }


  public void setCount(int count) {
    this.count = count;
  }

}
