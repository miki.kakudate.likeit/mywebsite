package beans;

import java.io.Serializable;

public class ProducerBeans implements Serializable {
  private int id;
  private int municipalId;
  private String name;
  private String imagePath;
  private String municipalName;
  private String prefectureName;

  public ProducerBeans() {
    super();
  }

  public ProducerBeans(int id, int municipalId, String name, String imagePath) {
    super();
    this.id = id;
    this.municipalId = municipalId;
    this.name = name;
    this.imagePath = imagePath;
  }

  public ProducerBeans(int id, int municipalId, String name, String imagePath,
      String municipalName2, String prefectureName2) {
    super();
    this.id = id;
    this.municipalId = municipalId;
    this.name = name;
    this.imagePath = imagePath;
    this.municipalName = municipalName2;
    this.prefectureName = prefectureName2;
  }

  public ProducerBeans(String municipalName, String prefectureName) {
    super();
    this.municipalName = municipalName;
    this.prefectureName = prefectureName;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public int getMunicipalId() {
    return municipalId;
  }

  public void setMunicipalId(int municipalId) {
    this.municipalId = municipalId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getImagePath() {
    return imagePath;
  }

  public void setImagePath(String imagePath) {
    this.imagePath = imagePath;
  }

  public String getMunicipalName() {
    return municipalName;
  }

  public void setMunicipalName(String municipalName) {
    this.municipalName = municipalName;
  }

  public String getPrefectureName() {
    return prefectureName;
  }

  public void setPrefectureName(String prefectureName) {
    this.prefectureName = prefectureName;
  }


}
