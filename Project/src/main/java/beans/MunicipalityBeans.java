package beans;

import java.io.Serializable;

public class MunicipalityBeans implements Serializable {
  private int id;
  private int prefectureId;
  private String name;
  private int order;

  public MunicipalityBeans() {
    super();
  }

  public MunicipalityBeans(int id, int prefectureId, String name, int order) {
    super();
    this.id = id;
    this.prefectureId = prefectureId;
    this.name = name;
    this.order = order;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public int getPrefectureId() {
    return prefectureId;
  }

  public void setPrefectureId(int prefectureId) {
    this.prefectureId = prefectureId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getOrder() {
    return order;
  }

  public void setOrder(int order) {
    this.order = order;
  }


}
