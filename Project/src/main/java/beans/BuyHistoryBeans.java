package beans;

import java.io.Serializable;
import java.sql.Timestamp;

public class BuyHistoryBeans implements Serializable {
  private int id;
  private int userId;
  private int deliveryId;
  private Timestamp buyDatetime;
  private int totalPrice;
  private String deliveryName;

  public BuyHistoryBeans() {
    super();
  }

  public BuyHistoryBeans(int id, int userId, int deliveryId, Timestamp buyDatetime,
      int totalPrice) {
    super();
    this.id = id;
    this.userId = userId;
    this.deliveryId = deliveryId;
    this.buyDatetime = buyDatetime;
    this.totalPrice = totalPrice;
  }

  public BuyHistoryBeans(int id, int userId, int deliveryId, Timestamp buyDatetime, int totalPrice,
      String deliveryName) {
    super();
    this.id = id;
    this.userId = userId;
    this.deliveryId = deliveryId;
    this.buyDatetime = buyDatetime;
    this.totalPrice = totalPrice;
    this.deliveryName = deliveryName;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }



  public int getUserId() {
    return userId;
  }

  public void setUserId(int userId) {
    this.userId = userId;
  }

  public int getDeliveryId() {
    return deliveryId;
  }

  public void setDeliveryId(int deliveryId) {
    this.deliveryId = deliveryId;
  }

  public Timestamp getBuyDatetime() {
    return buyDatetime;
  }

  public void setBuyDatetime(Timestamp buyDatetime) {
    this.buyDatetime = buyDatetime;
  }

  public int getTotalPrice() {
    return totalPrice;
  }

  public void setTotalPrice(int totalPrice) {
    this.totalPrice = totalPrice;
  }

  public String getDeliveryName() {
    return deliveryName;
  }

  public void setDeliveryName(String deliveryName) {
    this.deliveryName = deliveryName;
  }


}
