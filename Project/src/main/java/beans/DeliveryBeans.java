package beans;

import java.io.Serializable;

public class DeliveryBeans implements Serializable {
  private int id;
  private String name;
  private int price;
  private int order;

  public DeliveryBeans() {
    super();
  }

  public DeliveryBeans(int id, String name, int price, int order) {
    super();
    this.id = id;
    this.name = name;
    this.price = price;
    this.order = order;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getPrice() {
    return price;
  }

  public void setPrice(int price) {
    this.price = price;
  }

  public int getOrder() {
    return order;
  }

  public void setOrder(int order) {
    this.order = order;
  }


}
