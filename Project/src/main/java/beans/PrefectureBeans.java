package beans;

import java.io.Serializable;

public class PrefectureBeans implements Serializable {
  private int id;
  private String name;
  private int order;

  public PrefectureBeans() {
    super();
  }

  public PrefectureBeans(int id, String name, int order) {
    super();
    this.id = id;
    this.name = name;
    this.order = order;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getOrder() {
    return order;
  }

  public void setOrder(int order) {
    this.order = order;
  }


}
