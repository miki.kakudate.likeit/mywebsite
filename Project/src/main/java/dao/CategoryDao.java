package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import base.DBManager;
import beans.CategoryBeans;

public class CategoryDao {


  public static ArrayList<CategoryBeans> getAll() {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();
      String sql = "SELECT * FROM category";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      ResultSet rs = pStmt.executeQuery();

      ArrayList<CategoryBeans> categoryList = new ArrayList<CategoryBeans>();

      while (rs.next()) {
        CategoryBeans category = new CategoryBeans();
        category.setId(rs.getInt("id"));
        category.setName(rs.getString("name"));
        category.setOrder(rs.getInt("order"));
        categoryList.add(category);
      }
      return categoryList;
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

  public static CategoryBeans findById(int categoryId) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();
      String sql = "SELECT * FROM category WHERE id= ?";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, categoryId);
      ResultSet rs = pStmt.executeQuery();

      if (!rs.next()) {
        return null;
      }

      int id = rs.getInt("id");
      String name = rs.getString("name");
      int order = rs.getInt("order"); // 変数名がメソッドの引数と同じになってしまうため、別の名前を設定する
      return new CategoryBeans(id, name, order);

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }


}
