package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import base.DBManager;
import beans.UserBeans;

public class UserDao {
  public UserBeans findByLoginInfo(String loginId, String pass) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();
      String sql = "SELECT * FROM user WHERE mail = ? and password = ?";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      pStmt.setString(2, pass);
      ResultSet rs = pStmt.executeQuery();

      if (!rs.next()) {
        return null;
      }

      int id = rs.getInt("id");
      String name = rs.getString("name"); // 変数名がメソッドの引数と同じになってしまうため、別の名前(_loginId)を設定する
      String mail = rs.getString("mail");
      String _password = rs.getString("password"); // 変数名がメソッドの引数と同じになってしまうため、別の名前を設定する
      return new UserBeans(id, name, mail, _password);

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

  public static void userUpdate(String name, String mail, int id) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();
      String sql = " UPDATE user SET name=?, mail=? WHERE id = ?";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, name);
      pStmt.setString(2, mail);
      pStmt.setInt(3, id);
      pStmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();

    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }

  }

  public static UserBeans findById(int id) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();
      String sql = "SELECT * FROM user WHERE id=?";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, id);
      ResultSet rs = pStmt.executeQuery();

      if (!rs.next()) {
        return null;
      }

      int _id = rs.getInt("id");
      String name = rs.getString("name"); // 変数名がメソッドの引数と同じになってしまうため、別の名前(_loginId)を設定する
      String mail = rs.getString("mail");
      String _password = rs.getString("password"); // 変数名がメソッドの引数と同じになってしまうため、別の名前を設定する
      return new UserBeans(_id, name, mail, _password);

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

  public static void userRegist(String name, String mail, String inputPass) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();
      String sql = "INSERT INTO user(name,mail,password) VALUES(?,?,?)";
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, name);
      pStmt.setString(2, mail);
      pStmt.setString(3, inputPass);

      pStmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();

    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }

  }

}
