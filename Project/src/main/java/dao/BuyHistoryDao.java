package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import base.DBManager;
import beans.BuyHistoryBeans;

public class BuyHistoryDao {

  public static int insertBuyHistory(int userId, int deliveryId, Timestamp buyDate,
      int totalPrice) {
    Connection conn = null;
    int autoIncKey = -1;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      PreparedStatement pStmt = conn.prepareStatement(
          "INSERT INTO buy_history(user_id,delivery_id,buy_datetime,total_price) VALUES(?,?,?,?)",
          Statement.RETURN_GENERATED_KEYS);
      pStmt.setInt(1, userId);
      pStmt.setInt(2, deliveryId);
      pStmt.setTimestamp(3, buyDate);
      pStmt.setInt(4, totalPrice);
      pStmt.executeUpdate();

      ResultSet rs = pStmt.getGeneratedKeys();
      if (rs.next()) {
        autoIncKey = rs.getInt(1);
      }
      return autoIncKey;
    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
    return autoIncKey;
  }




  public static ArrayList<BuyHistoryBeans> findByUserId(int userId) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();
      String sql =
          "SELECT buy_history.*,delivery.name FROM buy_history INNER JOIN delivery ON buy_history.delivery_id=delivery.id WHERE buy_history.user_id=?";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, userId);
      ResultSet rs = pStmt.executeQuery();

      ArrayList<BuyHistoryBeans> buyHistoryList = new ArrayList<BuyHistoryBeans>();

      while (rs.next()) {
        BuyHistoryBeans buyHistory = new BuyHistoryBeans();
        buyHistory.setId(rs.getInt("id"));
        buyHistory.setUserId(rs.getInt("user_id"));
        buyHistory.setDeliveryId(rs.getInt("delivery_id"));
        buyHistory.setBuyDatetime(rs.getTimestamp("buy_datetime"));
        buyHistory.setTotalPrice(rs.getInt("total_price"));
        buyHistory.setDeliveryName(rs.getString("delivery.name"));
        buyHistoryList.add(buyHistory);
      }
      return buyHistoryList;
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }


  public static BuyHistoryBeans findById(int buyHistoryId) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();
      String sql =
          "SELECT buy_history.*,delivery.name FROM buy_history INNER JOIN delivery ON buy_history.delivery_id=delivery.id WHERE buy_history.id=?";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, buyHistoryId);
      ResultSet rs = pStmt.executeQuery();

      if (!rs.next()) {
        return null;
      }

      int id = rs.getInt("id");
      int user_id = rs.getInt("user_id");
      Timestamp buyDatetime = rs.getTimestamp("buy_datetime");
      int totalPrice = rs.getInt("total_price");
      int deliveryId = rs.getInt("delivery_id");
      String deliveryName = rs.getString("delivery.name");
      return new BuyHistoryBeans(id, user_id, deliveryId, buyDatetime, totalPrice, deliveryName);

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }



}
