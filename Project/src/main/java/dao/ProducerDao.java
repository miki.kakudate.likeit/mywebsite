package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import base.DBManager;
import beans.ProducerBeans;

public class ProducerDao {
  public static ArrayList<ProducerBeans> searchProducers(String searchWord) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();
      String sql =
          "SELECT producer.*,municipality.name,prefecture.name FROM producer JOIN municipality ON Producer.municipal_id=municipality.id RIGHT JOIN prefecture ON municipality.prefecture_id=prefecture.id WHERE producer.name LIKE ? OR prefecture.name LIKE ?";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, "%" + searchWord + "%");
      pStmt.setString(2, "%" + searchWord + "%");
      ResultSet rs = pStmt.executeQuery();

      ArrayList<ProducerBeans> searchResults = new ArrayList<ProducerBeans>();
      while (rs.next()) {
        ProducerBeans producer = new ProducerBeans();
        producer.setId(rs.getInt("id"));
        producer.setMunicipalId(rs.getInt("municipal_id"));
        producer.setName(rs.getString("name"));
        producer.setImagePath(rs.getString("image_path"));
        producer.setMunicipalName(rs.getString("municipality.name"));
        producer.setPrefectureName(rs.getString("prefecture.name"));
        searchResults.add(producer);
      }
      return searchResults;
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

  public static ArrayList<ProducerBeans> findAll() {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();
      String sql =
          "SELECT producer.*,municipality.name,prefecture.name FROM producer JOIN municipality ON Producer.municipal_id=municipality.id RIGHT JOIN prefecture ON municipality.prefecture_id=prefecture.id WHERE producer.municipal_id=municipality.id;";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      ResultSet rs = pStmt.executeQuery();

      ArrayList<ProducerBeans> ProducerList = new ArrayList<ProducerBeans>();
      while (rs.next()) {
        ProducerBeans producer = new ProducerBeans();
        producer.setId(rs.getInt("id"));
        producer.setMunicipalId(rs.getInt("municipal_id"));
        producer.setName(rs.getString("name"));
        producer.setImagePath(rs.getString("image_path"));
        producer.setMunicipalName(rs.getString("municipality.name"));
        producer.setPrefectureName(rs.getString("prefecture.name"));
        ProducerList.add(producer);
      }
      return ProducerList;
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

  public static ProducerBeans findByItemId(int itemId) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();
      String sql =
          "SELECT producer.*,producer.* FROM producer JOIN item ON producer.id=item.producer_id WHERE item.id=? ";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, itemId);
      ResultSet rs = pStmt.executeQuery();

      if (!rs.next()) {
        return null;
      }

      int id = rs.getInt("id");
      int municipalId = rs.getInt("municipal_id");
      String name = rs.getString("name");
      String imagePath = rs.getString("image_path");

      return new ProducerBeans(id, municipalId, name, imagePath);

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

  public static ProducerBeans findById(int producerId) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();
      String sql =
          " SELECT producer.*,municipality.name,prefecture.name FROM producer JOIN municipality ON producer.municipal_id=municipality.id RIGHT JOIN prefecture ON municipality.prefecture_id=prefecture.id WHERE producer.id=?";
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, producerId);
      ResultSet rs = pStmt.executeQuery();

      if (!rs.next()) {
        return null;
      }
      int id = rs.getInt("id");
      int municipalId = rs.getInt("municipal_id");
      String name = rs.getString("name");
      String imagePath = rs.getString("image_path");
      String municipalName=rs.getString("municipality.name");
      String prefectureName=rs.getString("prefecture.name");

      return new ProducerBeans(id, municipalId, name, imagePath,municipalName,prefectureName);

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }


}
