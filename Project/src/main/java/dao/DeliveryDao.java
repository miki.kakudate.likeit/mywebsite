package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import base.DBManager;
import beans.DeliveryBeans;

public class DeliveryDao {

  public static ArrayList<DeliveryBeans> findAll() {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();
      String sql = "SELECT * FROM delivery";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      ResultSet rs = pStmt.executeQuery();

      ArrayList<DeliveryBeans> deliveryList = new ArrayList<DeliveryBeans>();

      while (rs.next()) {
        DeliveryBeans delivery = new DeliveryBeans();
        delivery.setId(rs.getInt("id"));
        delivery.setName(rs.getString("name"));
        delivery.setPrice(rs.getInt("price"));
        delivery.setOrder(rs.getInt("order"));
        deliveryList.add(delivery);
      }
      return deliveryList;
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

  public static DeliveryBeans findById(int deliveryId) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();
      String sql = "SELECT * FROM delivery WHERE id= ?";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, deliveryId);
      ResultSet rs = pStmt.executeQuery();

      if (!rs.next()) {
        return null;
      }

      int id = rs.getInt("id");
      String name = rs.getString("name");
      int price = rs.getInt("price");
      int order = rs.getInt("order");
      return new DeliveryBeans(id, name, price, order);

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }

  }

}
