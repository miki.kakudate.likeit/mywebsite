package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import base.DBManager;
import beans.BuyItemBeans;

public class BuyItemDao {

  public static void insertBuyItem(BuyItemBeans buyItem) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();
      String sql = "INSERT INTO buy_item(item_id,count,buy_history_id) VALUES(?,?,?)";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, buyItem.getItemId());
      pStmt.setInt(2, buyItem.getCount());
      pStmt.setInt(3, buyItem.getBuyHistoryId());
      pStmt.executeUpdate();


    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  public static ArrayList<BuyItemBeans> findByBuyHistoryId(int buyHistoryId) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();
      String sql =
          "SELECT buy_item.*,item.name,item.price,item.unit,item.image_path FROM buy_item INNER JOIN item ON buy_item.item_id=item.id WHERE buy_history_id=?";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, buyHistoryId);
      ResultSet rs = pStmt.executeQuery();

      ArrayList<BuyItemBeans> buyItemList = new ArrayList<BuyItemBeans>();

      while (rs.next()) {
        BuyItemBeans buyItem = new BuyItemBeans();
        buyItem.setId(rs.getInt("id"));
        buyItem.setItemId(rs.getInt("item_id"));
        buyItem.setCount(rs.getInt("count"));
        buyItem.setBuyHistoryId(rs.getInt("buy_history_id"));
        buyItem.setItemPrice(rs.getInt("item.price"));
        buyItem.setItemName(rs.getString("item.name"));
        buyItem.setItemUnit(rs.getString("item.unit"));
        buyItem.setItemImagePath(rs.getString("item.image_path"));
        buyItemList.add(buyItem);
      }
      return buyItemList;
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }



}
