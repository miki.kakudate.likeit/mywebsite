package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import base.DBManager;
import beans.ItemsBeans;

public class ItemDao {
  public static ArrayList<ItemsBeans> getRandVegetables(int limit) throws SQLException {
    Connection con = null;
    PreparedStatement st = null;
    try {
      con = DBManager.getConnection();

      st = con
          .prepareStatement("SELECT * FROM item WHERE category_id <=10 ORDER BY RAND() LIMIT ? ");
      st.setInt(1, limit);

      ResultSet rs = st.executeQuery();

      ArrayList<ItemsBeans> vegeList = new ArrayList<ItemsBeans>();

      while (rs.next()) {
        ItemsBeans item = new ItemsBeans();
        item.setId(rs.getInt("id"));
        item.setProducerId(rs.getInt("producer_id"));
        item.setCategoryId(rs.getInt("category_id"));
        item.setName(rs.getString("name"));
        item.setDetail(rs.getString("detail"));
        item.setUnit(rs.getString("unit"));
        item.setPrice(rs.getInt("price"));
        item.setImagePath(rs.getString("image_path"));
        item.setImperfectItem(rs.getBoolean("is_imparfect_item"));
        vegeList.add(item);
      }
      return vegeList;
    } catch (SQLException e) {
      System.out.println(e.getMessage());
      throw new SQLException(e);
    } finally {
      if (con != null) {
        con.close();
      }
    }
  }

  public static ArrayList<ItemsBeans> getRandFruits(int limit) throws SQLException {
    Connection con = null;
    PreparedStatement st = null;
    try {
      con = DBManager.getConnection();

      st = con.prepareStatement("SELECT * FROM item WHERE category_id=11 ORDER BY RAND() LIMIT ? ");
      st.setInt(1, limit);

      ResultSet rs = st.executeQuery();

      ArrayList<ItemsBeans> fruitsList = new ArrayList<ItemsBeans>();

      while (rs.next()) {
        ItemsBeans item = new ItemsBeans();
        item.setId(rs.getInt("id"));
        item.setProducerId(rs.getInt("producer_id"));
        item.setCategoryId(rs.getInt("category_id"));
        item.setName(rs.getString("name"));
        item.setDetail(rs.getString("detail"));
        item.setUnit(rs.getString("unit"));
        item.setPrice(rs.getInt("price"));
        item.setImagePath(rs.getString("image_path"));
        item.setImperfectItem(rs.getBoolean("is_imparfect_item"));
        fruitsList.add(item);
      }
      return fruitsList;
    } catch (SQLException e) {
      System.out.println(e.getMessage());
      throw new SQLException(e);
    } finally {
      if (con != null) {
        con.close();
      }
    }
  }



  public static ArrayList<ItemsBeans> findByCategoryId(int categoryId) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();
      String sql = "SELECT * FROM item WHERE category_id= ?";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, categoryId);
      ResultSet rs = pStmt.executeQuery();

      ArrayList<ItemsBeans> indexList = new ArrayList<ItemsBeans>();
      while (rs.next()) {
        ItemsBeans item = new ItemsBeans();
        item.setId(rs.getInt("id"));
        item.setProducerId(rs.getInt("producer_id"));
        item.setCategoryId(rs.getInt("category_id"));
        item.setName(rs.getString("name"));
        item.setDetail(rs.getString("detail"));
        item.setUnit(rs.getString("unit"));
        item.setPrice(rs.getInt("price"));
        item.setImagePath(rs.getString("image_path"));
        item.setImperfectItem(rs.getBoolean("is_imparfect_item"));
        indexList.add(item);
      }
      return indexList;
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

  public static ItemsBeans findById(int itemId) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();
      String sql = "SELECT * FROM item WHERE id= ?";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, itemId);
      ResultSet rs = pStmt.executeQuery();

      if (!rs.next()) {
        return null;
      }

      int id = rs.getInt("id");
      int producerId = rs.getInt("producer_id");
      int categoryId = rs.getInt("category_id");
      String name = rs.getString("name");
      int price = rs.getInt("price");
      String detail = rs.getString("detail");
      String unit = rs.getString("unit");
      String imagePath = rs.getString("image_path");
      boolean isImperfectItem = rs.getBoolean("is_imparfect_item");
      return new ItemsBeans(id, producerId, categoryId, name, price, detail, unit, imagePath,
          isImperfectItem);

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

  public static int getTotalPrice(ArrayList<ItemsBeans> cartItem) {
    int total = 0;
    for (ItemsBeans item : cartItem) {
      total += (item.getPrice() * item.getCount());
    }
    return total;
  }

  public static int getTotalCount(ArrayList<ItemsBeans> cartItem) {
    int totalCount = 0;
    for (ItemsBeans item : cartItem) {
      totalCount += item.getCount();
    }
    return totalCount;
  }

  public static ArrayList<ItemsBeans> searchItems(String searchWord) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();
      String sql = "SELECT * FROM item WHERE name LIKE ? OR detail LIKE ?";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, "%" + searchWord + "%");
      pStmt.setString(2, "%" + searchWord + "%");
      ResultSet rs = pStmt.executeQuery();

      ArrayList<ItemsBeans> searchResults = new ArrayList<ItemsBeans>();
      while (rs.next()) {
        ItemsBeans item = new ItemsBeans();
        item.setId(rs.getInt("id"));
        item.setProducerId(rs.getInt("producer_id"));
        item.setCategoryId(rs.getInt("category_id"));
        item.setName(rs.getString("name"));
        item.setDetail(rs.getString("detail"));
        item.setUnit(rs.getString("unit"));
        item.setPrice(rs.getInt("price"));
        item.setImagePath(rs.getString("image_path"));
        item.setImperfectItem(rs.getBoolean("is_imparfect_item"));
        searchResults.add(item);
      }
      return searchResults;
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

  public static ArrayList<ItemsBeans> findImperfectitem(int categoryId) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();
      String sql = "SELECT * FROM item WHERE is_imparfect_item=1 AND category_id= ?";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, categoryId);
      ResultSet rs = pStmt.executeQuery();

      ArrayList<ItemsBeans> indexList = new ArrayList<ItemsBeans>();
      while (rs.next()) {
        ItemsBeans item = new ItemsBeans();
        item.setId(rs.getInt("id"));
        item.setProducerId(rs.getInt("producer_id"));
        item.setCategoryId(rs.getInt("category_id"));
        item.setName(rs.getString("name"));
        item.setDetail(rs.getString("detail"));
        item.setUnit(rs.getString("unit"));
        item.setPrice(rs.getInt("price"));
        item.setImagePath(rs.getString("image_path"));
        item.setImperfectItem(rs.getBoolean("is_imparfect_item"));
        indexList.add(item);
      }
      return indexList;
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

  public static ArrayList<ItemsBeans> findByProducerId(int producerId) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();
      String sql = "SELECT * FROM item WHERE producer_id=?";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, producerId);
      ResultSet rs = pStmt.executeQuery();

      ArrayList<ItemsBeans> indexList = new ArrayList<ItemsBeans>();
      while (rs.next()) {
        ItemsBeans item = new ItemsBeans();
        item.setId(rs.getInt("id"));
        item.setProducerId(rs.getInt("producer_id"));
        item.setCategoryId(rs.getInt("category_id"));
        item.setName(rs.getString("name"));
        item.setDetail(rs.getString("detail"));
        item.setUnit(rs.getString("unit"));
        item.setPrice(rs.getInt("price"));
        item.setImagePath(rs.getString("image_path"));
        item.setImperfectItem(rs.getBoolean("is_imparfect_item"));
        indexList.add(item);
      }
      return indexList;
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }
}