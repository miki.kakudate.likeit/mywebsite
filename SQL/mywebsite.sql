 CREATE DATABASE mywebsite DEFAULT CHARACTER SET utf8;
 USE mywebsite;
 
 --テーブル作成
 CREATE TABLE item(id int(11) PRIMARY KEY AUTO_INCREMENT,
 producer_id int(11) NOT NULL,
 category_id int(11) NOT NULL,
 name varchar(256) NOT NULL,
 price int(11) NOT NULL,
 detail text,
 unit varchar(256) NOT NULL ,
 image_path varchar(256) NOT NULL,
 is_imparfect_item boolean NOT NULL default false);
 
 CREATE TABLE category(id int(11) PRIMARY KEY AUTO_INCREMENT,
 name varchar(256) UNIQUE NOT NULL,
 `order` int(11) NOT NULL);
 
 CREATE TABLE producer(id int(11) PRIMARY KEY AUTO_INCREMENT,
 municipal_id int(11) NOT NULL,
 name varchar(256) NOT NULL,
 image_path varchar(256) NOT NULL);
 
 CREATE TABLE municipality(
    id int(11) PRIMARY KEY AUTO_INCREMENT,
    prefecture_id int(11) NOT NULL,
    name varchar(256) NOT NULL,
    `order` int(11) NOT NULL
)
;
CREATE TABLE prefecture(
    id int(11) PRIMARY KEY AUTO_INCREMENT,
    name varchar(256) UNIQUE NOT NULL,
    `order` int(11) NOT NULL
)
;
CREATE TABLE buy_item(
    id int(11) PRIMARY KEY AUTO_INCREMENT,
    item_id int(11) NOT NULL,
    count int(11) NOT NULL
)
;
CREATE TABLE buy_history(
    id int(11) PRIMARY KEY AUTO_INCREMENT,
    buy_item_id int(11) NOT NULL,
    user_id int(11) NOT NULL,
    delivery_id int(11) NOT NULL,
    buy_datetime date NOT NULL,
    total_price int(11) NOT NULL
)
;
CREATE TABLE user(
    id int(11) PRIMARY KEY AUTO_INCREMENT,
    name varchar(256) NOT NULL,
    mail varchar(256) NOT NULL,
    password varchar(256) NOT NULL
)
;
CREATE TABLE delivery(
    id int(11) PRIMARY KEY AUTO_INCREMENT,
    name varchar(256) UNIQUE NOT NULL,
    price int(11) NOT NULL,
    `order` int(11) NOT NULL
)
;
--------------------------------------------------------------------------------
INSERT INTO category(
    name,
    `order`
)
VALUES(
    '大根',
    1
)
;
INSERT INTO category(
    name,
    `order`
)
VALUES(
    '白菜',
    2
)
;
INSERT INTO category(
    name,
    `order`
)
VALUES(
    'きゅうり',
    3
)
;
INSERT INTO category(
    name,
    `order`
)
VALUES(
    '人参',
    4
)
;
INSERT INTO category(
    name,
    `order`
)
VALUES(
    'キャベツ',
    5
)
;
INSERT INTO category(
    name,
    `order`
)
VALUES(
    'トマト',
    6
)
;
INSERT INTO category(
    name,
    `order`
)
VALUES(
    'ごぼう',
    7
)
;
INSERT INTO category(
    name,
    `order`
)
VALUES(
    '玉ねぎ',
    8
)
;
INSERT INTO category(
    name,
    `order`
)
VALUES(
    'ナス',
    9
)
;
INSERT INTO category(
    name,
    `order`
)
VALUES(
    'その他',
    10
)
;
INSERT INTO category(
    name,
    `order`
)
VALUES(
    '果物',
    11
)
;
INSERT INTO prefecture(
    name,
    `order`
)
VALUES(
    '千葉県',
    1
)
;
INSERT INTO prefecture(
    name,
    `order`
)
VALUES(
    '青森県',
    2
)
;
INSERT INTO prefecture(
    name,
    `order`
)
VALUES(
    '茨城県',
    3
)
;
INSERT INTO prefecture(
    name,
    `order`
)
VALUES(
    '栃木県',
    4
)
;
INSERT INTO prefecture(
    name,
    `order`
)
VALUES(
    '長野県',
    5
)
;
INSERT INTO municipality(
    prefecture_id,
    name,
    `order`
)
VALUES(
    1,
    '松戸市',
     1
)
;
INSERT INTO municipality(
    prefecture_id,
    name,
    `order`
)
VALUES(
    1,
    '野田市',
    2
)
;
INSERT INTO municipality(
    prefecture_id,
    name,
    `order`
)
VALUES(
    2,
    '八戸市',
    3
)
;
INSERT INTO municipality(
    prefecture_id,
    name,
    `order`
)
VALUES(
    2,
    '青森市',
    4
)
;
INSERT INTO municipality(
    prefecture_id,
    name,
    `order`
)
VALUES(
    3,
    '茨城市',
    5
)
;
INSERT INTO municipality(
    prefecture_id,
    name,
    `order`
)
VALUES(
    3,
    '守谷市',
    6
)
;
INSERT INTO municipality(
    prefecture_id,
    name,
    `order`
)
VALUES(
    4,
    '栃木市',
    7
)
;
INSERT INTO municipality(
    prefecture_id,
    name,
    `order`
)
VALUES(
    4,
    '日光市',
    8
)
;
INSERT INTO municipality(
    prefecture_id,
    name,
    `order`
)
VALUES(
    5,
    '長野市',
    9
)
;
INSERT INTO producer(
    municipal_id,
    name,
    image_path
)
VALUES(
    1,
    '西田有事',
    'img/producer1.jpg'
)
;
INSERT INTO producer(
    municipal_id,
    name,
    image_path
)
VALUES(
    2,
    '小野寺太子',
    'img/producer2.jpg'
)
;
INSERT INTO producer(
    municipal_id,
    name,
    image_path
)
VALUES(
    3,
    '深津朝弘',
    'img/producer3.jpg'
)
;
INSERT INTO producer(
    municipal_id,
    name,
    image_path
)
VALUES(
    4,
    '大竹一誠',
    'img/producer4.jpg'
)
;
INSERT INTO producer(
    municipal_id,
    name,
    image_path
)
VALUES(
    5,
    '大塚辰徳',
    'img/producer5.jpg'
)
;
INSERT INTO producer(
    municipal_id,
    name,
    image_path
)
VALUES(
    6,
    '山内典弘',
    'img/producer6.jpg'
)
;
INSERT INTO producer(
    municipal_id,
    name,
    image_path
)
VALUES(
    7,
    '高梨健太',
    'img/producer7.jpg'
)
;
INSERT INTO producer(
    municipal_id,
    name,
    image_path
)
VALUES(
    8,
    '石川祐樹',
    'img/producer8.jpg'
)
;
INSERT INTO item(
    producer_id,
    category_id,
    name,
    price,
    detail,
    unit,
    image_path
)
VALUES(
    1,
    1,
    '大根',
    60,
    '・発送日の朝収穫新鮮野菜！・無農薬で安心安全！',
    '本',
    'img/daikon1.jpg'
)
;
INSERT INTO item(
    producer_id,
    category_id,
    name,
    price,
    detail,
    unit,
    image_path
)
VALUES(
    1,
    4,
    '人参　3本セット',
    120,
    '・発送日の朝収穫新鮮野菜！・無農薬で安心安全！',
    '袋',
    'img/ninjin1.jpg'
)
;
INSERT INTO item(
    producer_id,
    category_id,
    name,
    price,
    detail,
    unit,
    image_path
)
VALUES(
    2,
    2,
    '白菜',
    100,
    '・発送日の朝収穫新鮮野菜！・無農薬で安心安全！',
    '個',
    'img/hakusai1.jpg'
)
;
INSERT INTO item(
    producer_id,
    category_id,
    name,
    price,
    detail,
    unit,
    image_path
)
VALUES(
    2,
    3,
    'きゅうり　5本',
    150,
    '・発送日の朝収穫新鮮野菜！・無農薬で安心安全！',
    '袋',
    'img/kyuuri1.jpg'
)
;
INSERT INTO item(
    producer_id,
    category_id,
    name,
    price,
    detail,
    unit,
    image_path
)
VALUES(
    3,
    5,
    'キャベツ',
    100,
    '・発送日の朝収穫新鮮野菜！・無農薬で安心安全！',
    '玉',
    'img/kyabetu1.jpg'
)
;
INSERT INTO item(
    producer_id,
    category_id,
    name,
    price,
    detail,
    unit,
    image_path
)
VALUES(
    3,
    6,
    'トマト　3個入り',
    120,
    '・発送日の朝収穫新鮮野菜！・無農薬で安心安全！・甘くておいしい！',
    '袋',
    'img/tomato1.jpg'
)
;
INSERT INTO item(
    producer_id,
    category_id,
    name,
    price,
    detail,
    unit,
    image_path
)
VALUES(
    4,
    7,
    'ごぼう　3本',
    100,
    '・発送日の朝収穫新鮮野菜！・無農薬で安心安全！',
    '袋',
    'img/gobou1.jpg'
)
;
INSERT INTO item(
    producer_id,
    category_id,
    name,
    price,
    detail,
    unit,
    image_path
)
VALUES(
    4,
    8,
    '玉ねぎ　4個入り',
     150,
    '・発送日の朝収穫新鮮野菜！・無農薬で安心安全！',
    '袋',
    'img/tamanegi1.jpg'
)
;
INSERT INTO item(
    producer_id,
    category_id,
    name,
    price,
    detail,
    unit,
    image_path
)
VALUES(
    5,
    9,
    'ナス',
    60,
    '・発送日の朝収穫新鮮野菜！・無農薬で安心安全！・今が旬の野菜！',
    '束',
    'img/nasu1.jpg'
)
;
INSERT INTO item(
    producer_id,
    category_id,
    name,
    price,
    detail,
    unit,
    image_path
)
VALUES(
    5,
    10,
    '小松菜',
     60,
    '・発送日の朝収穫新鮮野菜！・無農薬で安心安全！',
    '本',
    'img/komatuna2.jpg'
)
;
INSERT INTO item(
    producer_id,
    category_id,
    name,
    price,
    detail,
    unit,
    image_path
)
VALUES(
    6,
    11,
    'りんご',
     100,
    '・発送日の朝収穫新鮮野菜！・無農薬で安心安全！',
    '個',
    'img/apple1.jpg'
)
;
INSERT INTO item(
    producer_id,
    category_id,
    name,
    price,
    detail,
    unit,
    image_path
)
VALUES(
    6,
    11,
    '桃' ,
    120,
    '・発送日の朝収穫新鮮野菜！・無農薬で安心安全！',
    '個',
    'img/momo1.jpg'
)
;
INSERT INTO item(
    producer_id,
    category_id,
    name,
    price,
    detail,
    unit,
    image_path
)
VALUES(
    7,
    11,
    'スイカ',
     960,
    '・発送日の朝収穫新鮮野菜！・無農薬で安心安全！',
    '玉',
    'img/suika1.jpg'
)
;
INSERT INTO item(
    producer_id,
    category_id,
    name,
    price,
    detail,
    unit,
    image_path
)
VALUES(
    7,
    11,
    'みかん　5個入り',
     780,
    '・発送日の朝収穫新鮮野菜！・無農薬で安心安全！',
    '袋',
    'img/orange1.jpg'
)
;
INSERT INTO item(
    producer_id,
    category_id,
    name,
    price,
    detail,
    unit,
    image_path
)
VALUES(
    8,
    11,
    'メロン',
     1200,
    '・発送日の朝収穫新鮮野菜！・無農薬で安心安全！',
    '玉',
    'img/meron1.jpg'
)
;
INSERT INTO item(
    producer_id,
    category_id,
    name,
    price,
    detail,
    unit,
    image_path,
    is_imparfect_item
)
VALUES(
    8,
    11,
    '梨 傷あり',
     250,
    '・発送日の朝収穫新鮮野菜！・無農薬で安心安全！',
    '個',
    'img/nashi1.jpg',
    1
)
;
INSERT INTO item(
    producer_id,
    category_id,
    name,
    price,
    detail,
    unit,
    image_path,
    is_imparfect_item
)
VALUES(
    1,
    1,
    '大根3本　傷あり',
     100,
    '・発送日の朝収穫新鮮野菜！・無農薬で安心安全！',
    '袋',
    'img/daikon2.jpg',
     1
)
;
INSERT INTO item(
    producer_id,
    category_id,
    name,
    price,
    detail,
    unit,
    image_path,
    is_imparfect_item
)
VALUES(
    2,
    3,
    'きゅうり　不揃い5本',
     120,
    '・発送日の朝収穫新鮮野菜！・無農薬で安心安全！',
    '袋',
    'img/kyuuri1.jpg',
     1
)
;
INSERT INTO item(
    producer_id,
    category_id,
    name,
    price,
    detail,
    unit,
    image_path,
    is_imparfect_item
)
VALUES(
    3,
    4,
    '人参　不揃い5本',
     150,
    '・発送日の朝収穫新鮮野菜！・無農薬で安心安全！',
    '袋',
    'img/ninjin2.jpg',
     1
)
;
INSERT INTO item(
    producer_id,
    category_id,
    name,
    price,
    detail,
    unit,
    image_path,
    is_imparfect_item
)
VALUES(
    4,
    5,
    'キャベツ　傷あり',
     80,
    '・発送日の朝収穫新鮮野菜！・無農薬で安心安全！',
    '玉',
    'img/kyabetu2.jpg',
     1
)
;
INSERT INTO delivery(
    name,
    price,
    `order`
)
VALUES(
    '通常配送',
    0,
    1
),
(
    '特急配送',
    500,
    2
),
(
    '日時指定配送',
    900,
    3
)
;
INSERT INTO user(
    name,
    mail,
    password
)
VALUES(
    '実験太郎',
    'taro.jikkenn@mail',
    'pass'
)
;

SELECT * FROM item WHERE category_id <=10 ORDER BY RAND() LIMIT 4;
UPDATE item SET unit='束' WHERE id=10;
